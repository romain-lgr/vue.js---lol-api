import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import PageNotFound from "./views/PageNotFound.vue";
import Summoner from "./views/Summoner.vue";
import Champions from "./views/Champions.vue";
import Champion from "./views/Champion.vue";
import Ladder from "./views/Ladder.vue";

Vue.use(Router);
export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/404",
      name: "PageNotFound",
      component: PageNotFound
    },
    {
      path: "/summoner/:id",
      name: "Summoner",
      component: Summoner
    },
    {
      path: "/champions/",
      name: "Champions",
      component: Champions
    },
    {
      path: "/champions/:champion",
      component: Champion,
      name: "Champion"
    },
    {
      path: "/ladder/",
      component: Ladder,
      name: "Ladder"
    },
    {
      path: "*",
      redirect: "/404"
    }
  ]
});