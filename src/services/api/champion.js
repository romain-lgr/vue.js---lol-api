import axios from "axios";

export function getChampion(slug) {
  return axios
    .get('/static-data/data/en_US/champion/' + slug + '.json')
    .then(response => {
      return response.data;
    })
}
