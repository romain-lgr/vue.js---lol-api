import axios from "axios";

export function getChampions() {
  return axios
    .get('/static-data/data/en_US/championFull.json')
    .then(response => {
      return response.data;
    })
}

// https://ddragon.leagueoflegends.com/realms/euw.json