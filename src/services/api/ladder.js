import axios from "axios";
import {
  apiKey
} from "@/services/api/apiKey.js";

const proxy = 'https://cors-anywhere.herokuapp.com/';
const challengersUrl = "https://euw1.api.riotgames.com/lol/league/v4/challengerleagues/by-queue/RANKED_SOLO_5x5/";
const grandMasterUrl = "https://euw1.api.riotgames.com/lol/league/v4/grandmasterleagues/by-queue/RANKED_SOLO_5x5/";

export function getChallengers() {
  return axios
    .get(proxy + challengersUrl + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}

export function getGrandMaster() {
  return axios
    .get(proxy + grandMasterUrl + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}

// Merge and order ladder data