import axios from "axios";
import {
  apiKey
} from "@/services/api/apiKey.js";

const proxy = 'https://cors-anywhere.herokuapp.com/';
const matchListUrl = "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/"; 
const matchUrl = "https://euw1.api.riotgames.com/lol/match/v4/matches/"; 

export function getMatchList(id) {
  return axios
    .get(proxy + matchListUrl + id + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}

export function getMatch(id) {
  return axios
    .get(proxy + matchUrl + id + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}