import axios from "axios";
import {
  apiKey
} from "@/services/api/apiKey.js";

const proxy = 'https://cors-anywhere.herokuapp.com/';
const url = "https://euw1.api.riotgames.com/lol/status/v3/shard-data"; 

export function getStatus() {
  return axios
    .get(proxy + url + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}
