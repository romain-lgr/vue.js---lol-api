import axios from "axios";
import {
  apiKey
} from "@/services/api/apiKey.js";

const proxy = 'https://cors-anywhere.herokuapp.com/';
const url = "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/"; 

export function getSummonerName(name) {
  return axios
    .get(proxy + url + name + "?api_key=" + apiKey)
    .then(response => {
      return response.data;
    })
}
