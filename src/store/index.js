import Vue from "vue";
import Vuex from "vuex";
import champions from "./modules/champions";
import status from "./modules/status";
import ladder from "./modules/ladder";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    champions,
    status,
    ladder
  }
});
