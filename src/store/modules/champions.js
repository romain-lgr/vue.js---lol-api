import {
  getChampions
} from "@/services/api/champions.js";

export default {
  state: {
    champions: []
  },
  mutations: {
    setChampions(state, champions) {
      state.champions = champions
    }
  },
  actions: {
    loadChampions({
      commit
    }) {
      return getChampions()
        .then(champions => {
          commit('setChampions', champions)
        })
    }
  }
}