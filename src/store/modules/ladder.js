import {
  getChallengers,
  getGrandMaster
} from "@/services/api/ladder.js";
import _ from "lodash";

export default {
  state: {
    isLoading: false,
    hasError: false,
    ladder: [],
    challengers: [],
    grandMaster: []
  },
  mutations: {
    setLoading(state, payload) {
      state.isLoading = payload;
    },
    setError(state, payload) {
      state.hasError = payload;
    },
    setChallengers(state, challengers) {
      state.challengers = challengers
    },
    setGrandMaster(state, grandMaster) {
      state.grandMaster = grandMaster
    },
    setLadder(state, ladder) {
      state.ladder = ladder
    }
  },
  actions: {
    loadChallengers({
      commit
    }) {
      commit("setLoading", true);
      commit("setError", false);
      return getChallengers()
        .then(challengers => {
          commit('setChallengers', challengers)
        })
        .catch(error => {
          commit("setError", true);
        })
        .finally(() => {
          commit("setLoading", false);
        });
    },
    loadGrandMaster({
      commit
    }) {
      commit("setLoading", true);
      commit("setError", false);
      return getGrandMaster()
        .then(grandMaster => {
          commit('setGrandMaster', grandMaster)
        })
        .catch(error => {
          commit("setError", true);
        })
        .finally(() => {
          commit("setLoading", false);
        });
    }
  },
  getters: {
    getChallengers: state => {
      return _.orderBy(state.challengers.entries, "leaguePoints", "desc");
    }
  }
}