import {
  getStatus
} from "@/services/api/status.js";

export default {
  state: {
    status: []
  },
  mutations: {
    setStatus(state, status) {
      state.status = status
    }
  },
  actions: {
    loadStatus({
      commit
    }) {
      return getStatus()
        .then(status => {
          commit('setStatus', status)
        })
    }
  }
}